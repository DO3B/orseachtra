package vue;

import javax.swing.*;
/**
 * Classe FenetrePopup
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class FenetrePopup {
    /**
     * Constructeur FenetrePopup
     * @param message message
     * @param erreur boolean
     */
    public FenetrePopup(String message, boolean erreur){
        if(erreur){
            JOptionPane.showMessageDialog(new JFrame(),
                    message,
                    "Erreur",
                    JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(new JFrame(),
                    message,
                    "Succès",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
