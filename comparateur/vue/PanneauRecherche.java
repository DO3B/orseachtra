package vue;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.*;

import controlleur.Recherche;
import controlleur.musique.Album;
import controlleur.musique.Artiste;
import controlleur.musique.Genre;
import controlleur.musique.Musique;

/**
 * Classe PanneauRecherche
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class PanneauRecherche extends JPanel {
	private JComboBox<Genre> cbGenres = new JComboBox<>();
    private JTextField tfNomM;
    private JTextField tfNomAr;
    private JTextField tfNomAl;
    private JTextField tfAnnee;
    private JSpinner tfNote;
    private ButtonGroup btnDuree;

    /**
     * Constructeur de PanneauRecherche
     * @param type Default - Recherche, 2 - Modifier, 3 - Supprimer
     */
	public PanneauRecherche(int type){
        setLayout(new GridBagLayout());
        String titre = "Comparez vos morceaux";
        switch (type){/** Default - Recherche, 2 - Modifier, 3 - Supprimer **/
            case 2:
                titre = "Modifiez vos morceaux";
                break;
            case 3:
                titre = "Supprimez vos morceaux";
                break;
            default:
                break;
        }
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(titre),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(0,0,2,5);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        JLabel genre = new JLabel("Genre* :");
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(genre, gbc);

        ArrayList<Genre> listeGenre = Genre.getAllGenres();
        for(Genre g:listeGenre){
            cbGenres.addItem(g);
        }
        gbc.gridx = 2;
        gbc.gridy = 1;
        add(cbGenres, gbc);

        JLabel nomM = new JLabel("Nom du morceau :");
        gbc.gridx = 1;
        gbc.gridy = 2;
        add(nomM, gbc);

        tfNomM = new JTextField(20);
        gbc.gridx = 2;
        gbc.gridy = 2;
        add(tfNomM, gbc);


        JLabel nomAr = new JLabel("Artiste :");
        gbc.gridx = 1;
        gbc.gridy = 3;
        add(nomAr, gbc);

        tfNomAr = new JTextField(20);
        gbc.gridx = 2;
        gbc.gridy = 3;
        add(tfNomAr, gbc);

        JLabel nomAl = new JLabel("Album :");
        gbc.gridx = 1;
        gbc.gridy = 4;
        add(nomAl, gbc);

        tfNomAl = new JTextField(20);
        gbc.gridx = 2;
        gbc.gridy = 4;
        add(tfNomAl, gbc);

        JLabel annee = new JLabel("Annee :");
        gbc.gridx = 1;
        gbc.gridy = 5;
        add(annee, gbc);

        tfAnnee = new JTextField(4);
        gbc.gridx = 2;
        gbc.gridy = 5;
        add(tfAnnee, gbc);

        JLabel duree = new JLabel("Duree :");
        gbc.gridx = 1;
        gbc.gridy = 6;
        add(duree, gbc);

        JPanel panneauDuree = new JPanel();
        panneauDuree.setLayout(new FlowLayout());
        btnDuree = new ButtonGroup();
        JRadioButton court = new JRadioButton("Courte");
        btnDuree.add(court);
        panneauDuree.add(court);

        JRadioButton moyenne = new JRadioButton("Moyenne");
        btnDuree.add(moyenne);
        panneauDuree.add(moyenne);

        JRadioButton longue = new JRadioButton("Longue");
        btnDuree.add(longue);
        panneauDuree.add(longue);

        gbc.gridx = 2;
        gbc.gridy = 6;
        add(panneauDuree,gbc);

        JLabel note = new JLabel("Note :");
        gbc.gridx = 1;
        gbc.gridy = 7;
        add(note, gbc);

        String[] handSpinnerValue = new String[]{"Non","0","1","2","3","4","5"};
        SpinnerModel handSpinner = new SpinnerListModel(handSpinnerValue);
        tfNote = new JSpinner(handSpinner);
        tfNote.setValue("Non");
        gbc.gridx = 2;
        gbc.gridy = 7;
        add(tfNote, gbc);

        JLabel info = new JLabel("Les champs avec une * sont obligatoires");
        gbc.gridx = 1;
        gbc.gridwidth = 2;
        gbc.gridy = 8;
        gbc.insets = new Insets(10,115,2,5);
        gbc.anchor = GridBagConstraints.EAST;
        add(info,gbc);
    }

    /**
     * Valide les champs
     * @param nbDeRetour nombre de retour
     * @return ArrayList Musiques post-recherche
     */
	public ArrayList<Musique> Valider(int nbDeRetour) {

        if (cbGenres.getSelectedItem() != null) {
            Genre genre = (Genre) cbGenres.getSelectedItem();
            String nomM;
            Artiste artiste;
            Album album;
            if(tfNomM.getText().equals("")){
                nomM = null;
            } else {
                nomM = tfNomM.getText();
            }
            if(tfNomAr.getText().equals("")){
                artiste = null;
            } else {
                artiste = new Artiste(tfNomAr.getText());
            }
            if(tfNomAl.getText().equals("")){
                album = null;
            } else {
                album = new Album(tfNomAl.getText());
            }

            SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
            Date date = null;
            if(!tfAnnee.getText().equals("")) {
                try {
                    Integer annee = Integer.valueOf(tfAnnee.getText());
                    date = formatAnnee.parse(annee.toString());
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }

            String duree = null;
            for (Enumeration<AbstractButton> buttons = btnDuree.getElements(); buttons.hasMoreElements();) {
                AbstractButton button = buttons.nextElement();
                if (button.isSelected()) {
                    duree = button.getText();
                }
            }

            char cDuree = 'v';
            if(duree != null) {
                switch (duree) {
                    case "Courte":
                        cDuree = 'c';
                        break;
                    case "Moyenne":
                        cDuree = 'm';
                        break;
                    case "Longue":
                        cDuree = 'l';
                        break;
                }
            }

            int note = -1;
            if(!tfNote.getValue().equals("Non")) {
                note = Integer.parseInt(tfNote.getValue().toString());
            }

            Musique musiqueR = new Musique(genre,
                    nomM,
                    artiste,
                    album,
                    date,
                    cDuree,
                    note
            );
            
            return Recherche.lancerRecherche(musiqueR, nbDeRetour);
        }
        return null;

	}
}
