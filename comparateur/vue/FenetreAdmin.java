package vue;

import controlleur.musique.Musique;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe FenetreAdmin
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class FenetreAdmin extends JFrame {
    private JTabbedPane onglet;
    private PanneauAdministration ajout;
    private PanneauRecherche modif;
    private PanneauRecherche suppr;

    /**
     * Constructeur de FenetreAdmin
     */
    public FenetreAdmin(){

        try {
            ImageIcon icone = new ImageIcon(ImageIO.read(getClass().getResource("/resources/icone.png")));
            this.setIconImage(icone.getImage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        setLocationRelativeTo(null);
        setTitle("Orsearchtra - Administration de la BDD");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(550, 300);

        ajout = new PanneauAdministration();
        modif = new PanneauRecherche(2);
        suppr = new PanneauRecherche(3);

        onglet = new JTabbedPane(JTabbedPane.LEFT);

        onglet.add("Ajouter", ajout);
        onglet.add("Modifier", modif);
        onglet.add("Supprimer", suppr);

        JButton modifButton = new JButton("Rechercher");
        JButton supprButton = new JButton("Rechercher");

        modifButton.addActionListener(new ResultatModifAction());
        supprButton.addActionListener(new ResultatSupprAction());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 9;
        modif.add(modifButton, gbc);
        suppr.add(supprButton, gbc);

        getContentPane().add(onglet);
        pack();
        setVisible(true);

    }

    /**
     * Classe ResultatModifAction
     */
    public class ResultatModifAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ArrayList<Musique> res = modif.Valider(-1);
            new FenetreResultats(res,2);
        }
    }

    /**
     * Classe ResultatSupprAction
     */
    public class ResultatSupprAction implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            ArrayList<Musique> res = suppr.Valider(-1);
            new FenetreResultats(res, 3);
        }
    }
}