package vue;

import controlleur.Utile;
import controlleur.musique.Album;
import controlleur.musique.Artiste;
import controlleur.musique.Genre;
import controlleur.musique.Musique;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * Classe FenetreResultats
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class FenetreResultats extends JFrame {
    private ArrayList<Musique> listeResultat;
    private ArrayList<Musique> listeModifier;
    private HashMap<Musique, Musique> hsMusique = new HashMap<>();
    private ImageIcon icone;

    /**
     * Constructeur de FenetreResultats
     * @param listeMusique liste contenant les résultats
     * @param type de résultat : recherche, édition ou suppression
     */
    public FenetreResultats(ArrayList<Musique> listeMusique, int type){
        try {
            ImageIcon icone = new ImageIcon(ImageIO.read(getClass().getResource("/resources/icone.png")));
            this.setIconImage(icone.getImage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        setLayout(new BorderLayout(10,10));
        listeResultat = listeMusique;

        setTitle("Orsearchtra - Résultats de votre recherche");
        setSize(700,200);
        setResizable(false);

        JPanel tableau = new JPanel();
        switch(type){ /* 1 - Recherche; 2 - Modification; 3 - Suppression */
            case 1:
                tableau = new PanneauTableauRecherche();
                break;
            case 2:
                tableau = new PanneauTableauModifier();
                break;
            case 3:
                tableau = new PanneauTableauSupprimer();
                break;
        }


        add(tableau,BorderLayout.CENTER);
        if(type == 2){
            add(new BoutonPanneau(true),BorderLayout.SOUTH);
        } else {
            add(new BoutonPanneau(false),BorderLayout.SOUTH);
        }

        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        pack();
    }

    /**
     * Classe du panneau content les résultats de la recherche
     */
    private  class PanneauTableauRecherche extends JPanel {
        /**
         * Constructeur de PanneauTableau
         */
        private PanneauTableauRecherche(){
            JTable t = tableauResultat(1);
            iconeAction("/resources/plus.png",t);

            JScrollPane jscroll = new JScrollPane(t);
            add(jscroll);
        }
    }

    /**
     * Classe PanneauTableauModifier
     */
    private class PanneauTableauModifier extends JPanel {

        /**
         * Constructeur de PanneauTableauModifier
         */
        private PanneauTableauModifier(){
            listeModifier = new ArrayList<>();
            for(Musique m : listeResultat){
                listeModifier.add(new Musique(m));
            }
            JTable t = new JTable(new TableauModifierMusique());
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment( JLabel.CENTER );
            TableColumnModel tcm = t.getColumnModel();
            t.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
            t.setFillsViewportHeight(true);


            TableColumn tcGenre = t.getColumnModel().getColumn(0);
            ArrayList<Genre> listeGenre = Genre.getAllGenres();
            Java2sAutoComboBox cbGenres = new Java2sAutoComboBox(listeGenre);
            cbGenres.setDataList(listeGenre);
            cbGenres.setMaximumRowCount(5);
            tcm.getColumn(0).setPreferredWidth(100);
            tcGenre.setCellEditor(new DefaultCellEditor(cbGenres));

            ajusterLargeurColonne(t, 1);

            TableColumn tcArtiste = t.getColumnModel().getColumn(2);
            ArrayList<Artiste> listeArtiste = Artiste.getAllArtistes();
            Java2sAutoComboBox cbArtistes = new Java2sAutoComboBox(listeArtiste);
            cbArtistes.setDataList(listeArtiste);
            cbArtistes.setMaximumRowCount(5);
            tcArtiste.setCellEditor(new DefaultCellEditor(cbArtistes));

            ajusterLargeurColonne(t, 3);
            TableColumn tcAlbum = t.getColumnModel().getColumn(3);
            ArrayList<Album> listeAlbum = Album.getAllAlbums();
            Java2sAutoComboBox cbAlbum = new Java2sAutoComboBox(listeAlbum);
            cbAlbum.setDataList(listeAlbum);
            cbAlbum.setMaximumRowCount(5);
            tcAlbum.setCellEditor(new DefaultCellEditor(cbAlbum));

            tcm.getColumn(4).setCellRenderer(centerRenderer);
            tcm.getColumn(4).setPreferredWidth(50);

            tcm.getColumn(5).setCellRenderer(centerRenderer);
            String[] duree = {"Courte","Moyenne","Longue"};
            JComboBox cbDuree = new JComboBox(duree);
            TableColumn tcDuree = t.getColumnModel().getColumn(5);
            tcDuree.setCellEditor(new DefaultCellEditor(cbDuree));

            tcm.getColumn(6).setCellRenderer(centerRenderer);
            String[] handSpinnerValue = new String[]{"0","1","2","3","4","5"};
            JComboBox cbNote = new JComboBox(handSpinnerValue);
            TableColumn tcNote = t.getColumnModel().getColumn(6);
            tcNote.setCellEditor(new DefaultCellEditor(cbNote));
            tcm.getColumn(6).setPreferredWidth(45);

            ajusterLargeurColonne(t, 7);

            TableColumn tcModifier = t.getColumnModel().getColumn(8);
            tcModifier.setCellRenderer(t.getDefaultRenderer(Boolean.class));
            tcModifier.setCellEditor(t.getDefaultEditor(Boolean.class));
            tcm.getColumn(8).setPreferredWidth(60);
            //iconeAction("/resources/modify.png",t);

            t.setPreferredScrollableViewportSize(new Dimension(t.getPreferredSize().width,t.getRowHeight() * 10));
            JScrollPane jscroll = new JScrollPane(t);
            add(jscroll);
        }
    }

    /**
     * Classe PanneauTableauSupprimer
     */
    private class PanneauTableauSupprimer extends JPanel {
        /**
         * Constructeur de PanneauTableauSupprimer
         */
        private PanneauTableauSupprimer(){
            JTable t = tableauResultat(3);
            iconeAction("/resources/delete.png",t);
            JScrollPane jscroll = new JScrollPane(t);
            add(jscroll);
        }
    }

    /**
     * Créé une JTable selon le type d'action
     * @param type l'action
     * @return JTable
     */
    private JTable tableauResultat(int type){
        JTable t = new JTable(new TableauResultatMusique(type));

        t.getColumn(t.getColumnName(7)).setCellRenderer(new ButtonRenderer());

        // Tentative de mise en forme de la JTable ALED
        t.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        t.setFillsViewportHeight(true);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        TableColumnModel tcm = t.getColumnModel();
        // N°
        tcm.getColumn(0).setPreferredWidth(35);
        tcm.getColumn(0).setCellRenderer(centerRenderer);
        // Nom
        ajusterLargeurColonne(t, 1);
        // Album
        ajusterLargeurColonne(t, 2);
        // Artiste
        ajusterLargeurColonne(t, 3);
        // Année
        tcm.getColumn(4).setCellRenderer(centerRenderer);
        // Durée
        tcm.getColumn(5).setCellRenderer(centerRenderer);
        // Note
        tcm.getColumn(6).setCellRenderer(centerRenderer);
        tcm.getColumn(7).setPreferredWidth(45);
        t.addMouseListener(new TableauAction(t));
        t.setPreferredScrollableViewportSize(new Dimension(t.getPreferredSize().width,t.getRowHeight() * 5));

        return t;
    }

    /**
     * Permet de redéfinir l'icone du tableau t.
     * @param lien lien vers l'image
     * @param t le tableau
     */
    private void iconeAction(String lien, JTable t){
        try {
            icone = new ImageIcon(ImageIO.read(getClass().getResource(lien)));
            Image image = icone.getImage();
            image = image.getScaledInstance(t.getRowHeight() - 1, t.getRowHeight() - 1,Image.SCALE_SMOOTH);
            icone = new ImageIcon(image);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Ajuste la largeur des colonnes pour qu'elle s'adapte à la plus grande ligne.
     * @param t le tableau que l'on souhaite modifier
     * @param column la colonne que l'on modifie
     */
    private void ajusterLargeurColonne (JTable t, int column) {
        TableColumnModel tcm = t.getColumnModel();
        int width = 15; // Min width
        for (int row = 0; row < t.getRowCount(); row++) {
            TableCellRenderer renderer = t.getCellRenderer(row, column);
            Component comp = t.prepareRenderer(renderer, row, column);
            width = Math.max(comp.getPreferredSize().width + 5 , width);
        }
        if(width > 300)
            width=300;
        tcm.getColumn(column).setPreferredWidth(width);
    }

    /**
     * Classe contenant le bouton Retour
     */
    private class BoutonPanneau extends JPanel {
        /**
         * Constructeur BoutonRetour
         * @param modifier vrai si c'est une modification
         */
        private BoutonPanneau(boolean modifier){
            JButton bRetour = new JButton("Retour");
            setLayout(new FlowLayout());
            bRetour.addActionListener(new BoutonListener());
            if(modifier){
                JButton bValider = new JButton("Valider");
                bValider.addActionListener(new BoutonListener());
                add(bValider);
            }
            add(bRetour);
        }
    }

    /**
     * Classe TableauResultatMusique contenant les modifications du modèle
     */
    private class TableauResultatMusique extends AbstractTableModel{
        private String[] entetes = {"N°","Nom","Artiste","Album","Année","Durée","Note","Action"};
        private Object[][] donnees;
        private int typeAction;

        /**
         * Constructeur du nouveau modèle
         * @param t type de recherche 1 comparaison, 2 édition,3
         */
        private TableauResultatMusique(int t){
            donnees = new Object[listeResultat.size()][8];
            this.typeAction = t;
        }

        @Override
        public int getRowCount(){
            return donnees.length;
        }

        @Override
        public int getColumnCount(){
            return entetes.length;
        }

        public String getColumnName(int columnIndex) {
            return entetes[columnIndex];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                return donnees[rowIndex][0] = rowIndex + 1;
            } else {
                switch (columnIndex) {
                    case 1:
                        donnees[rowIndex][1] = listeResultat.get(rowIndex).getNom();
                        break;
                    case 2:
                        donnees[rowIndex][2] = listeResultat.get(rowIndex).getArtiste();
                        break;
                    case 3:
                        donnees[rowIndex][3] = listeResultat.get(rowIndex).getAlbum().getNom();
                        break;
                    case 4:
                        LocalDate localDate = listeResultat.get(rowIndex).getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        int annee = localDate.getYear();
                        donnees[rowIndex][4] = annee;
                        break;
                    case 5:
                        donnees[rowIndex][5] = Utile.convertirDuree(listeResultat.get(rowIndex).getDuree());
                        break;
                    case 6:
                        donnees[rowIndex][6] = listeResultat.get(rowIndex).getNote() + "/5";
                        break;
                    case 7:
                        JButton boutonAction = new JButton(icone);
                        switch (typeAction){
                            case 1:
                                boutonAction.setBackground(Color.GREEN);
                                break;
                            case 2:
                                break;
                            case 3:
                                boutonAction.setBackground(Color.RED);
                                break;
                        }
                        boutonAction.setBorderPainted(false);
                        boutonAction.addActionListener(new Action(listeResultat,listeResultat.get(rowIndex), typeAction));
                        donnees[rowIndex][7] = boutonAction;
                        break;
                    default:
                        break;
                }
                return donnees[rowIndex][columnIndex];
            }
        }

        /**
         * Classe permettant d'exécuter l'action liée au bouton du tableau
         */
        private class Action implements ActionListener {
            private ArrayList<Musique> arrayM;
            private Musique musique;
            private int typeAction;

            private Action(ArrayList<Musique> arrayM, Musique m, int t){
                this.arrayM = arrayM;
                this.musique = m;
                this.typeAction = t;
            }
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (typeAction){
                    case 1:
                        FenetreDetails details =  new FenetreDetails(musique);
                        break;
                    case 3:
                        Object[] options = {"Oui", "Non"};
                        int n = JOptionPane.showOptionDialog(new JFrame(), "<html>Voulez-vous vraiment supprimer <strong>" + musique + "</strong> de l'album <strong>" +  musique.getAlbum().getNom() + " </strong> ?",
                                "Supprimer un morceau",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                options,
                                options[1]);
                        if(n == 0){
                            musique.supprimerDansLaBDD();
                            arrayM.remove(musique);
                            donnees = new Object[listeResultat.size()][8];
                        }
                        break;
                }
            }
        }
    }

    /**
     * Classe TableauModifierMusique
     */
    private  class TableauModifierMusique extends AbstractTableModel {
        private String[] entetes = {"Genre","Nom","Artiste","Album","Année","Durée","Note","Lien","Modifier ?"};
        private Object[][] donnees;

        /**
         * Constructeur TableauModifierMusique
         */
        private TableauModifierMusique(){
            donnees = new Object[listeModifier.size()][9];

            for(int i = 0; i < listeModifier.size(); i++){
                donnees[i][8] = false;
            }
        }

        @Override
        public int getRowCount() {
            return donnees.length;
        }

        @Override
        public int getColumnCount() {
            return entetes.length;
        }

        public String getColumnName(int columnIndex) {
            return entetes[columnIndex];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                return donnees[rowIndex][0] = listeModifier.get(rowIndex).getGenre();
            } else {
                switch (columnIndex) {
                    case 1:
                        donnees[rowIndex][1] = listeModifier.get(rowIndex).getNom();
                        break;
                    case 2:
                        donnees[rowIndex][2] = listeModifier.get(rowIndex).getArtiste();
                        break;
                    case 3:
                        donnees[rowIndex][3] = listeModifier.get(rowIndex).getAlbum().getNom();
                        break;
                    case 4:
                        LocalDate localDate = listeModifier.get(rowIndex).getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        int annee = localDate.getYear();
                        donnees[rowIndex][4] = annee;
                        break;
                    case 5:
                        donnees[rowIndex][5] = Utile.convertirDuree(listeModifier.get(rowIndex).getDuree());
                        break;
                    case 6:
                        donnees[rowIndex][6] = listeModifier.get(rowIndex).getNote();
                        break;
                    case 7:
                        donnees[rowIndex][7] = listeModifier.get(rowIndex).getLien();
                        break;
                    case 8:
                        donnees[rowIndex][8] = donnees[rowIndex][columnIndex];
                        break;
                    default:
                        break;
                }

                return donnees[rowIndex][columnIndex];
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            Musique row = listeModifier.get(rowIndex);
            boolean modifier = false;

            switch (columnIndex){
                case 0:
                    Genre genre;
                    if(aValue instanceof String){
                        genre = new Genre((String) aValue);
                    } else {
                        genre = (Genre) aValue;
                    }
                    if(!genre.equals(listeResultat.get(rowIndex).getGenre())){
                        modifier = true;
                    }
                    row.setGenre(genre);
                    break;
                case 1:
                    String nom = (String) aValue;
                    if(!nom.equals(listeResultat.get(rowIndex).getNom())){
                        modifier = true;
                    }
                    row.setNom(nom);
                    break;
                case 2:
                    Artiste artiste;
                    if(aValue instanceof String){
                        artiste = new Artiste((String) aValue);
                    } else {
                        artiste = (Artiste) aValue;
                    }

                    if(!artiste.getNom().equals(listeResultat.get(rowIndex).getArtiste().getNom())){
                        modifier = true;
                    }

                    row.setArtiste(artiste);
                    break;
                case 3:
                    Album album;
                    if(aValue instanceof String){
                        album = new Album((String) aValue);
                    } else {
                        album = (Album) aValue;
                    }

                    if(!album.getNom().equals(listeResultat.get(rowIndex).getAlbum().getNom())){
                        modifier = true;
                    }

                    row.setAlbum(album);
                    break;
                case 4:
                    Date date = null;
                    SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
                    try {
                        date = formatAnnee.parse(aValue.toString());
                    } catch (ParseException e){
                        e.printStackTrace();
                    }

                    if(!date.equals(listeResultat.get(rowIndex).getAnnee())){
                        modifier = true;
                    }

                    row.setAnnee(date);
                    break;
                case 5:
                    char duree = 'v';
                    switch ((String) aValue) {
                        case "Courte":
                            duree = 'c';
                            break;
                        case "Moyenne":
                            duree = 'm';
                            break;
                        case "Longue":
                            duree = 'l';
                            break;
                    }

                    if(duree != listeResultat.get(rowIndex).getDuree()){
                        modifier = true;
                    }

                    row.setDuree(duree);
                    break;
                case 6:
                    int note = Integer.parseInt((String) aValue);

                    if (note != listeResultat.get(rowIndex).getNote()){
                        modifier = true;
                    }

                    row.setNote(note);
                    break;
                case 7:
                    String lien = (String) aValue;
                    if(!lien.equals(listeResultat.get(rowIndex).getLien())){
                        modifier = true;
                    }
                    break;
                case 8:
                    modifier = (Boolean) aValue;
                    if(!modifier){
                        hsMusique.remove(listeResultat.get(rowIndex));
                    } else {
                        hsMusique.put(listeResultat.get(rowIndex), listeModifier.get(rowIndex));
                    }
                    donnees[rowIndex][columnIndex] = modifier;
                    modifier = false;
                    break;
                default:
                    break;
            }

            if(modifier){
                this.setValueAt(true, rowIndex, 8);
            }

        }

        public boolean isCellEditable(int row, int col) {
            if(col == 8){
                return true;
            } else {
                return (Boolean) donnees[row][8];
            }
        }
    }

    /**
     * Classe permettant d'afficher le bouton dans une cellule.
     */
    public class ButtonRenderer extends JButton implements TableCellRenderer{
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if(value instanceof JButton) {
                return (JButton) value;
            }
            return this;
        }
    }

    /**
     * Classe permettant de cliquer sur les boutons contenus dans des cellules
     */
    private class TableauAction extends MouseAdapter {
        JTable table;

        private TableauAction(JTable t){
            this.table = t;
        }
        @Override
        public void mouseClicked(MouseEvent evt) {
            int row = table.rowAtPoint(evt.getPoint());
            int col = table.columnAtPoint(evt.getPoint());
            Object value = table.getValueAt(row, col);
            if(value instanceof JButton) {
                ((JButton) value).doClick();
            }
        }
    }

    /**
     * Classe BoutonListener
     */
    private class BoutonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("Valider")){
                Object[] options = {"Oui", "Non"};
                int n = JOptionPane.showOptionDialog(new JFrame()
                        ,"<html>Souhaitez-vous vraiment modifier "+ (hsMusique.size() > 1 ? "les <strong>" + hsMusique.size() + "</strong> morceaux cochés" :  "le morceau coché")+" ?</html>",
                        "Modifier",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[1]);
                if(n == 0){
                    for(Musique mOld : hsMusique.keySet()){
                        mOld.modifierDansLaBDD(hsMusique.get(mOld));
                    }
                    dispose();
                }
            } else {
                dispose();
            }
        }
    }
}
