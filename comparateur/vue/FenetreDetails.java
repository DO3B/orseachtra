package vue;

import controlleur.Utile;
import controlleur.musique.Musique;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Classe FenetreDetails
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class FenetreDetails extends JFrame {
    private Musique musique;
    private Image image;

    /**
     * Constructeur de FenetreDetails
     * @param m musique
     */
    public FenetreDetails(Musique m){

        try {
            ImageIcon icone = new ImageIcon(ImageIO.read(getClass().getResource("/resources/icone.png")));
            this.setIconImage(icone.getImage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.musique = m;

        setLocationRelativeTo(null);
        setTitle(musique.getNom());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(300, 400));

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // AlbumImage
        JLabel imageAlbum = null;
        if(musique.getAlbum().getCouverture() != null) {
            try {
                URL url = new URL(musique.getAlbum().getCouverture());
                image = ImageIO.read(url);
                image = image.getScaledInstance(200, 200,Image.SCALE_SMOOTH);
            } catch (IOException e){
                e.printStackTrace();
            }
            imageAlbum = new JLabel(new ImageIcon(image));
        } else {
            imageAlbum = new JLabel("Indisponible");
            imageAlbum.setHorizontalAlignment(SwingConstants.CENTER);
            imageAlbum.setPreferredSize(new Dimension(200, 200));
        }

        imageAlbum.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        gbc.gridy = 1;
        gbc.gridwidth = 5;
        gbc.gridheight = 1;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.insets = new Insets(5, 5, 0, 0);
        add(imageAlbum, gbc);

        // Titre
        gbc.gridx = 1;
        gbc.gridwidth = 5;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(5, 0, 0, 0);
        JLabel titreMusique = new JLabel(musique.getNom());
        titreMusique.setFont(new Font("Arial", Font.BOLD, 16));
        add(titreMusique, gbc);

        // Album
        gbc.gridx = 1;
        gbc.gridwidth = 5;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);
        JLabel albumMusique = new JLabel( musique.getAlbum().getNom());
        albumMusique.setFont(new Font("Arial", Font.ITALIC, 13));
        add(albumMusique, gbc);


        // Artiste
        gbc.gridx = 1;
        gbc.gridwidth = 5;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);
        JLabel auteurMusique = new JLabel( musique.getArtiste().getNom());
        add(auteurMusique, gbc);

        // Année
        gbc.gridx = 1;
        gbc.gridwidth = 5;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);
        LocalDate localDate = musique.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int annee = localDate.getYear();
        JLabel anneeMusique = new JLabel( String.valueOf(annee));
        anneeMusique.setFont(new Font("Arial", Font.PLAIN, 12));
        add(anneeMusique, gbc);

        //Boutons
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.insets = new Insets(5, 0, 0, 0);
        add(new PanneauBouton(), gbc);
        setVisible(true);
    }

    /**
     * Classe PanneauBouton
     */
    private class PanneauBouton extends JPanel {

        /**
         * Constructeur de PanneauBouton
         */
        private PanneauBouton(){
            ImageIcon headphone = null;
            try {
                headphone= new ImageIcon(ImageIO.read(getClass().getResource("/resources/headphone.png")));
                Image image = headphone.getImage();
                image = image.getScaledInstance(15, 15,Image.SCALE_SMOOTH);
                headphone = new ImageIcon(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JButton jEcouter = new JButton("Ecouter",headphone);
            JButton jRetour = new JButton("Retour");
            jEcouter.addActionListener(new BoutonListener());
            jRetour.addActionListener(new BoutonListener());
            add(jEcouter);
            add(jRetour);
        }
    }

    /**
     * Classe BoutonListener
     */
    private class BoutonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("Ecouter")){
                Utile.ouvrirLien(musique.getLien());
            } else {
                dispose();
            }
        }
    }

}
