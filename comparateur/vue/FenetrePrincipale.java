package vue;

import javax.imageio.ImageIO;
import javax.swing.*;

import controlleur.musique.Musique;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe FenetrePrincipale
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class FenetrePrincipale extends JFrame {

	private CardLayout layout = new CardLayout();
	private JPanel content = new JPanel();
	private String[] listePanneaux = {"Menu", "Recherche", "ConnexionAdmin"};
	
	private PanneauMenu panneauMenu;
	private PanneauRecherche panneauRecherche;
	private PanneauLoginAdmin panneauLoginAdmin;
	
	private JButton searchB ;
	private JButton searchConfirmB ;
	private JButton retSearchB ;
	private JButton adminLoginB ;
	private JButton confirmAdminLoginB ;
	private JButton cancelAdminLoginB ;

    /**
     * Constructeur de FenetrePrincipale
     */
    public FenetrePrincipale(){

        try {
            ImageIcon icone = new ImageIcon(ImageIO.read(getClass().getResource("/resources/icone.png")));
            this.setIconImage(icone.getImage());
        } catch (IOException e) {
            e.printStackTrace();
        }


        ImageIcon search = null;
        ImageIcon admin = null;
        Image menuImage = null;

        setTitle("Orsearchtra - Comparez des morceaux musicaux");
        setSize(500,300);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        // Thème de Windows, à essayer sous Mac.
        try {
            UIManager.setLookAndFeel(
                    UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        /*Images*/
        try {
            admin= new ImageIcon(ImageIO.read(getClass().getResource("/resources/admin.png")));
            Image image = admin.getImage();
            image = image.getScaledInstance(15, 15,Image.SCALE_SMOOTH);
            admin = new ImageIcon(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            search= new ImageIcon(ImageIO.read(getClass().getResource("/resources/search.png")));
            Image image = search.getImage();
            image = image.getScaledInstance(15, 15,Image.SCALE_SMOOTH);
            search = new ImageIcon(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            menuImage= new ImageIcon(ImageIO.read(getClass().getResource("/resources/orsearchtra.png"))).getImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*Fin Images*/
        
        panneauMenu = new PanneauMenu();
        panneauRecherche = new PanneauRecherche(1);
        panneauLoginAdmin = new PanneauLoginAdmin();

        searchB = new JButton("Recherche", search);
        searchConfirmB = new JButton("Rechercher");
        retSearchB = new JButton("Retour");
        adminLoginB = new JButton("Espace d'Administration", admin);
        confirmAdminLoginB = new JButton("Connexion");
        cancelAdminLoginB = new JButton("Annuler");

        searchB.addActionListener(new SwapRechercheAction());
        searchConfirmB.addActionListener(new SwapResultatAction());
        retSearchB.addActionListener(new SwapMenuAction());
        adminLoginB.addActionListener(new SwapAdminLoginAction());
        confirmAdminLoginB.addActionListener(new SwapAdminAction());
        cancelAdminLoginB.addActionListener(new SwapMenuAction());

        panneauMenu.setLayout(new BorderLayout());
        JPanel panneauMenuBoutons = new JPanel();
        panneauMenuBoutons.add(searchB);
        panneauMenuBoutons.add(adminLoginB);
        JLabel menuIcon = new JLabel(new ImageIcon(menuImage));
        panneauMenu.add(menuIcon, BorderLayout.CENTER);
        panneauMenu.add(panneauMenuBoutons, BorderLayout.SOUTH);

        JPanel panneauRechercheBoutons = new JPanel();
        panneauRechercheBoutons.add(searchConfirmB);
        panneauRechercheBoutons.add(retSearchB);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 9;
        panneauRecherche.add(panneauRechercheBoutons, gbc);


        JPanel panneauLoginAdminBoutons = new JPanel();
        panneauLoginAdminBoutons.add(confirmAdminLoginB);
        panneauLoginAdminBoutons.add(cancelAdminLoginB);
        gbc.gridx = 1;
        gbc.gridy = 3;
        panneauLoginAdmin.add(panneauLoginAdminBoutons, gbc );
        
        content.setLayout(layout);
        content.add(panneauMenu, listePanneaux[0]);
        content.add(panneauRecherche, listePanneaux[1]);
        content.add(panneauLoginAdmin, listePanneaux[2]);


        this.getContentPane().add(content, BorderLayout.CENTER);

        this.setVisible(true);

    }

    /**
     * Classe SwapMenuAction
     */
    public class SwapMenuAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			layout.show(content, listePanneaux[0]);
		}
    }

    /**
     * Classe SwapRechercheAction
     */
    public class SwapRechercheAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			layout.show(content, listePanneaux[1]);
		}
    }

    /**
     * Classe SwapResultatAction
     */
    public class SwapResultatAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			ArrayList<Musique> res = panneauRecherche.Valider(5);
			new FenetreResultats(res, 1);
		}
    }

    /**
     * SwapAdminLoginAction
     */
    public class SwapAdminLoginAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			layout.show(content, listePanneaux[2]);
		}    	
    }

    /**
     * Classe SwapAdminAction
     */
    public class SwapAdminAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
		    if( panneauLoginAdmin.isMDPCorrect() )
                new FenetreAdmin();
		    else
                JOptionPane.showMessageDialog(new JFrame(),
                        "Mot de passe incorrect.",
                        "Erreur d'identification",
                        JOptionPane.ERROR_MESSAGE);
		}
    }
}

