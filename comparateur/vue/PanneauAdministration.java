package vue;

import controlleur.musique.Album;
import controlleur.musique.Artiste;
import controlleur.musique.Genre;
import controlleur.musique.Musique;
import modele.ArtisteModele;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

/**
 * Classe PanneauAdministration
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class PanneauAdministration extends JPanel {
    private CardLayout layout = new CardLayout();
    private String[] listeAjout = {"Ajouter","Artiste","Album","Musique"};

    private JButton artisteB;
    private JButton albumB;
    private JButton musiqueB;
    private JButton retourB;

    private PanneauAjouterMusique panneauMusique;
    private JPanel content = new JPanel();

    /**
     * Constructeur de PanneauAdministration
     */
    public PanneauAdministration(){
        artisteB = new JButton("Ajouter un Artiste");
        albumB = new JButton("Ajouter un Album");
        musiqueB = new JButton("Ajouter une Musique");
        retourB = new JButton("Retour");

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        JPanel panneauAction = new JPanel();
        panneauAction.add(artisteB);
        panneauAction.add(albumB);
        panneauAction.add(musiqueB);
        add(panneauAction,gbc);

        artisteB.addActionListener(new SwapAjoutArtiste());
        albumB.addActionListener(new SwapAjoutAlbum());
        musiqueB.addActionListener(new SwapAjoutMusique());

        content.setLayout(layout);
        content.add(panneauAction, listeAjout[0]);
        content.add(new PanneauAjouterArtiste(), listeAjout[1]);
        content.add(new PanneauAjouterAlbum(), listeAjout[2]);

        add(content,gbc);
    }

    /**
     * Classe PanneauAjouterArtiste
     */
    private class PanneauAjouterArtiste extends JPanel {
        private JTextField tfArtiste;

        /**
         * Constructeur PanneauAjouterArtiste
         */
        private PanneauAjouterArtiste() {
            setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();

            JLabel informations = new JLabel("Remplissez le champ ci-dessous pour ajouter un nouvel artiste.");
            gbc.gridx = 1;
            gbc.gridy = 1;
            gbc.insets = new Insets(0, 0, 10, 0);
            add(informations,gbc);

            JLabel artisteNom = new JLabel("Nom de l'Artiste :");
            gbc.gridx = 1;
            gbc.gridy = 2;
            gbc.insets = new Insets(0, 0, 0, 0);
            gbc.anchor = GridBagConstraints.CENTER;
            add(artisteNom,gbc);

            tfArtiste = new JTextField(20);
            gbc.gridx = 1;
            gbc.gridy = 3;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(tfArtiste,gbc);

            gbc.gridx = 1;
            gbc.gridy = 4;
            add(new PanneauBoutons(), gbc);
        }

        /**
         * Indique si les champs sont bien tous remplis
         * @return boolean
         */
        private boolean champsRemplis(){
            return !tfArtiste.getText().equals("");
        }
    }

    /**
     * Classe PanneauAjouterAlbum
     */
    private class PanneauAjouterAlbum extends JPanel {
        private JTextField tfTitreAlbum;
        private JSpinner tfNbPistes;
        private JComboBox typeCB;
        private JTextField tfCouverture;

        /**
         * Constructeur de PanneauAjouterAlbum
         */
        private PanneauAjouterAlbum() {
            setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.fill = GridBagConstraints.HORIZONTAL;

            JLabel informations = new JLabel("Remplissez les champs ci-dessous pour ajouter un nouvel album.");
            gbc.gridwidth = 2;
            gbc.gridx = 1;
            gbc.gridy = 1;
            gbc.insets = new Insets(0, 0, 10, 0);
            add(informations,gbc);

            // Titre
            JLabel albumNom = new JLabel("Titre :");
            gbc.gridwidth = 1;
            gbc.gridx = 1;
            gbc.gridy = 2;
            gbc.insets = new Insets(0, 0, 0, 0);
            add(albumNom,gbc);

            tfTitreAlbum = new JTextField(20);
            gbc.gridx = 2;
            gbc.gridy = 2;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(tfTitreAlbum,gbc);

            // Nombre de pistes
            JLabel nbPistes = new JLabel("Nombre de pistes :");
            gbc.gridx = 1;
            gbc.gridy = 3;
            gbc.insets = new Insets(0, 0, 0, 0);
            add(nbPistes,gbc);

            SpinnerModel handSpinner = new SpinnerNumberModel(1, 1, 50, 1);
            tfNbPistes = new JSpinner(handSpinner);
            gbc.gridx = 2;
            gbc.gridy = 3;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(tfNbPistes,gbc);

            // Type
            JLabel type = new JLabel("Type :");
            gbc.gridx = 1;
            gbc.gridy = 4;
            gbc.insets = new Insets(0, 0, 0, 0);
            add(type,gbc);

            String[] typeAlbum = {"EP","LP","Album","Single"};
            typeCB = new JComboBox(typeAlbum);
            gbc.gridx = 2;
            gbc.gridy = 4;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(typeCB,gbc);

            // Couverture
            JLabel urlCouverture = new JLabel("Couverture :");
            gbc.gridx = 1;
            gbc.gridy = 5;
            gbc.insets = new Insets(0, 0, 0, 0);
            add(urlCouverture,gbc);

            tfCouverture = new JTextField(20);
            gbc.gridx = 2;
            gbc.gridy = 5;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(tfCouverture,gbc);

            gbc.gridwidth = 2;
            gbc.gridx = 1;
            gbc.gridy = 6;
            add(new PanneauBoutons(), gbc);
        }

        private boolean champsRemplis(){
            return !tfTitreAlbum.getText().equals("")
                    && !tfCouverture.getText().equals("");
        }
    }

    private class PanneauAjouterMusique extends JPanel {
        private Java2sAutoComboBox cbGenres;
        private JTextField tfNomM;
        private Java2sAutoComboBox cbNomAr;
        private Java2sAutoComboBox cbNomAl;
        private JTextField tfAnnee;
        private JSpinner tfNote;
        private ButtonGroup btnDuree;
        private JTextField tfUrl;

        private PanneauAjouterMusique() {
            setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(0,5,2,0);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            JLabel informations = new JLabel("Remplissez tous les champs ci-dessous pour ajouter une nouvelle musique.");
            gbc.gridwidth = 2;
            gbc.gridx = 1;
            gbc.gridy = 0;
            add(informations, gbc);

            JLabel nomM = new JLabel("Titre :");
            gbc.gridwidth = 1;
            gbc.gridx = 1;
            gbc.gridy = 1;
            add(nomM, gbc);

            tfNomM = new JTextField(20);
            gbc.gridx = 2;
            gbc.gridy = 1;
            add(tfNomM, gbc);

            JLabel genre = new JLabel("Genre :");
            gbc.gridx = 1;
            gbc.gridy = 2;
            add(genre, gbc);

            ArrayList<Genre> listeGenre = Genre.getAllGenres();
            cbGenres = new Java2sAutoComboBox(listeGenre);
            cbGenres.setDataList(listeGenre);
            gbc.gridx = 2;
            gbc.gridy = 2;
            add(cbGenres, gbc);

            JLabel nomAr = new JLabel("Artiste :");
            gbc.gridx = 1;
            gbc.gridy = 3;
            add(nomAr, gbc);

            ArrayList<Artiste> listeArtiste = Artiste.getAllArtistes();
            cbNomAr = new Java2sAutoComboBox(listeArtiste);
            cbNomAr.setDataList(listeArtiste);
            gbc.gridx = 2;
            gbc.gridy = 3;
            add(cbNomAr, gbc);

            JLabel nomAl = new JLabel("Album :");
            gbc.gridx = 1;
            gbc.gridy = 4;
            add(nomAl, gbc);

            ArrayList<Album> listeAlbum = Album.getAllAlbums();
            cbNomAl = new Java2sAutoComboBox(listeAlbum);
            cbNomAl.setDataList(listeAlbum);
            gbc.gridx = 2;
            gbc.gridy = 4;
            add(cbNomAl, gbc);

            JLabel annee = new JLabel("Année :");
            gbc.gridx = 1;
            gbc.gridy = 5;
            add(annee, gbc);

            tfAnnee = new JTextField(4);
            gbc.gridx = 2;
            gbc.gridy = 5;
            add(tfAnnee, gbc);

            JLabel duree = new JLabel("Durée :");
            gbc.gridx = 1;
            gbc.gridy = 6;
            add(duree, gbc);

            JPanel panneauDuree = new JPanel();
            panneauDuree.setLayout(new FlowLayout());
            btnDuree = new ButtonGroup();
            JRadioButton court = new JRadioButton("Courte");
            btnDuree.add(court);
            panneauDuree.add(court);

            JRadioButton moyenne = new JRadioButton("Moyenne");
            btnDuree.add(moyenne);
            panneauDuree.add(moyenne);

            JRadioButton longue = new JRadioButton("Longue");
            btnDuree.add(longue);
            panneauDuree.add(longue);

            gbc.gridx = 2;
            gbc.gridy = 6;
            add(panneauDuree,gbc);

            JLabel note = new JLabel("Note :");
            gbc.gridx = 1;
            gbc.gridy = 7;
            add(note, gbc);

            SpinnerNumberModel handSpinner = new SpinnerNumberModel(0, 0, 5, 1);
            tfNote = new JSpinner(handSpinner);
            gbc.gridx = 2;
            gbc.gridy = 7;
            add(tfNote, gbc);

            JLabel url = new JLabel("Lien :");
            gbc.gridx = 1;
            gbc.gridy = 8;
            add(url, gbc);

            tfUrl = new JTextField(20);
            gbc.gridx = 2;
            gbc.gridy = 8;
            add(tfUrl, gbc);

            gbc.gridwidth = 2;
            gbc.gridx = 1;
            gbc.gridy = 9;
            add(new PanneauBoutons(), gbc);
        }

        /**
         * Indique si les champs sont bien tous remplis
         * @return boolean
         */
        private boolean champsRemplis(){
            return !tfAnnee.getText().equals("")
                    && !tfNomM.getText().equals("")
                    && !tfUrl.getText().equals("");
        }
    }

    /**
     * Classe PanneauBoutons
     */
    private class PanneauBoutons extends JPanel {

        /**
         * Constructeur de PanneauBoutons
         */
        private PanneauBoutons(){
            JButton validerB = new JButton("Valider");
            validerB.addActionListener(new ActionValider());
            add(validerB);
            retourB = new JButton("Retour");
            retourB.addActionListener(new SwapRetourAction());
            add(retourB);
        }
    }

    /**
     * Classe ActionValider
     */
    public class ActionValider implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
                JButton bouton = (JButton) e.getSource();
                JPanel parent = (JPanel) bouton.getParent().getParent();
                if(parent instanceof PanneauAjouterArtiste){
                    if(((PanneauAjouterArtiste) parent).champsRemplis()){
                        String nomArtiste = ((PanneauAjouterArtiste) parent).tfArtiste.getText();
                        Artiste aNew = new Artiste(nomArtiste);
                        aNew.ajouterALaBDD();
                        Artiste.majajourArtiste();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(),
                                "Il faut remplir le champ avant de valider.",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } else if (parent instanceof PanneauAjouterAlbum){
                    if(((PanneauAjouterAlbum) parent).champsRemplis()){
                        String titreAlbum = ((PanneauAjouterAlbum) parent).tfTitreAlbum.getText();
                        String nbPistes = ((PanneauAjouterAlbum) parent).tfNbPistes.getValue().toString();
                        String typeAlbum = ((PanneauAjouterAlbum) parent).typeCB.getSelectedItem().toString();
                        String urlCouverture = ((PanneauAjouterAlbum) parent).tfCouverture.getText();
                        Album aNew = new Album(titreAlbum, Integer.parseInt(nbPistes), typeAlbum, urlCouverture);
                        aNew.ajouterALaBDD();
                        Album.majajourAlbum();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(),
                                "Il faut remplir tous les champs avant de valider.",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    }

                } else if (parent instanceof PanneauAjouterMusique){
                    if(((PanneauAjouterMusique) parent).champsRemplis()){
                        String nomMusique = ((PanneauAjouterMusique) parent).tfNomM.getText();
                        String genreMusique = ((PanneauAjouterMusique) parent).cbGenres.getSelectedItem().toString();
                        String albumNom = ((PanneauAjouterMusique) parent).cbNomAl.getSelectedItem().toString();
                        String artisteMusique =  ((PanneauAjouterMusique) parent).cbNomAr.getSelectedItem().toString();

                        String anneeMusique = ((PanneauAjouterMusique) parent).tfAnnee.getText();
                        SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
                        Date date = null;
                        if(!anneeMusique.equals("")) {
                            try {
                                Integer annee = Integer.valueOf(anneeMusique);
                                date = formatAnnee.parse(annee.toString());
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                        }

                        String duree = null;
                        ButtonGroup btnDuree = ((PanneauAjouterMusique) parent).btnDuree;
                        for (Enumeration<AbstractButton> buttons = btnDuree.getElements(); buttons.hasMoreElements();) {
                            AbstractButton button = buttons.nextElement();
                            if (button.isSelected()) {
                                duree = button.getText();
                            }
                        }

                        char cDuree = 'v';
                        if(duree != null) {
                            switch (duree) {
                                case "Courte":
                                    cDuree = 'c';
                                    break;
                                case "Moyenne":
                                    cDuree = 'm';
                                    break;
                                case "Longue":
                                    cDuree = 'l';
                                    break;
                            }
                        }

                        int note = -1;
                        JSpinner tfNote = ((PanneauAjouterMusique) parent).tfNote;
                        if(!tfNote.getValue().equals("")) {
                            note = Integer.parseInt(tfNote.getValue().toString());
                        }

                        String url = ((PanneauAjouterMusique) parent).tfUrl.getText();

                        Genre genreM = new Genre(genreMusique);
                        Artiste artisteM = new Artiste(artisteMusique);
                        Album albumM = new Album(albumNom);

                        Musique mNew = new Musique(genreM,
                                nomMusique,
                                artisteM,
                                albumM,
                                date,
                                cDuree,
                                note,
                                url
                        );

                        mNew.ajouterALaBDD();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(),
                                "Il faut remplir tous les champs avant de valider.",
                                "Erreur",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }

        }
    }

    /**
     * Classe SwapRetourAction
     */
    public class SwapRetourAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            layout.show(content, listeAjout[0]);
        }
    }

    /**
     * Classe SwapAjoutArtiste
     */
    public class SwapAjoutArtiste implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            layout.show(content, listeAjout[1]);
        }
    }

    /**
     * Classe SwapAjoutAlbum
     */
    public class SwapAjoutAlbum implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            layout.show(content, listeAjout[2]);
        }
    }

    /**
     * Classe SwapAjoutMusique
     */
    public class SwapAjoutMusique implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            panneauMusique = new PanneauAjouterMusique();
            content.add(panneauMusique, listeAjout[3]);
            layout.show(content, listeAjout[3]);
        }
    }
}
