package vue;

import javax.swing.*;

import controlleur.Admin;

import java.awt.*;

/**
 * Classe PanneauLoginAdmin
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class PanneauLoginAdmin extends JPanel{
    private Admin admin;
    private JPasswordField mdp;

    /**
     * Constructeur de PanneauLoginAdmin
     */
    public PanneauLoginAdmin(){
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        JLabel mdpL = new JLabel("Mot de passe administrateur : ");
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(mdpL, gbc);
        mdp = new JPasswordField(15);
        gbc.gridx = 1;
        gbc.gridy = 2;
        add(mdp, gbc);
    }

    /**
     * Verifie si le mdp est correct
     * @return boolean
     */
    public boolean isMDPCorrect(){
        admin = new Admin(String.valueOf(mdp.getPassword()));
        if(!admin.isMDPCorrect()) mdp.setText("");
        return( admin.isMDPCorrect());
    }
}
