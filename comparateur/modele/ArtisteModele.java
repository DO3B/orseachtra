package modele;

import controlleur.Utile;
import controlleur.musique.Artiste;

import java.sql.*;
import java.util.*;
import java.util.Map.Entry;

/**
 * Classe ArtisteModele
 * Elle permet de faire la liaison entre la table Artiste en BDD et l'objet Artiste
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class ArtisteModele {
    private static TreeMap<Integer, Artiste> listeArtistes = new TreeMap<>();


    /**
     * Retourne tout les Artistes présents en BDD
     * @return ArrayList
     **/
    public static ArrayList<Artiste> getArtistes(){
        Statement sql;
        try {
            sql = ConnexionBD.getInstance().createStatement();

            ResultSet result = sql.executeQuery("SELECT * FROM Artiste");

            ArrayList<Artiste> retour = new ArrayList<>();

            while (result.next()) {
                Artiste a = new Artiste(result.getObject(2).toString());
                int id  = Integer.parseInt(result.getObject(1).toString());

                listeArtistes.put(id, a);
                retour.add(a);
            }

            result.close();
            sql.close();

            return retour;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retourne le Artiste qui possède l'id i en BDD
     * @param i id
     * @return Artiste
     **/
    public static Artiste getArtisteFromId(int i) {
        if(listeArtistes.isEmpty()) getArtistes();

        return listeArtistes.get(i);
    }

    /**
     * Retourne l'id du Artiste en fonction de son nom
     * @param a Artiste
     * @return id
     **/
    public static int getIdFromArtiste(Artiste a) {
        if(listeArtistes.isEmpty()) getArtistes();

        for (Entry<Integer, Artiste> unArtiste : listeArtistes.entrySet()) {
            if (unArtiste.getValue().equals(a)) {
                return unArtiste.getKey();
            }
        }
        return 0;
    }

    /**
     * Ajouter l'artiste a dans la base de données.
     * @param a Artiste
     * @throws BDException erreur en BDD
     */
    public static void creerArtiste(Artiste a) throws BDException{
        if(listeArtistes.isEmpty()) getArtistes();
        if(!listeArtistes.containsValue(a)){
            String requete = "INSERT INTO Artiste (Nom) VALUES (?);";
            PreparedStatement prepare;
            try {
                prepare = ConnexionBD.getInstance().prepareStatement(requete);

                prepare.setString(1, Utile.capitaliserTitre(a.getNom()));
                prepare.executeUpdate();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            // MaJ à jour de la liste
            getArtistes();
        } else {
            throw new BDException(a + " existe déjà dans la base de données");
        }
    }

    /**
     * Vérifie si la TreeMap est vide.
     * @return boolean
     */
    public static boolean isEmpty(){
        return listeArtistes.isEmpty();
    }
}