package modele;

import controlleur.Utile;
import controlleur.musique.Album;

import java.sql.*;
import java.util.*;
import java.util.Map.Entry;

/**
 * Classe AlbumModele
 * Elle permet de faire la liaison entre la table Album en BDD et l'objet Album
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class AlbumModele {
    private static TreeMap<Integer, Album> listeAlbums = new TreeMap<>();


    /**
     * Retourne tout les Albums présents en BDD
     * @return ArrayList
     **/
    public static ArrayList<Album> getAlbums(){
        Statement sql;
        try {
            sql = ConnexionBD.getInstance().createStatement();

            ResultSet result = sql.executeQuery("SELECT * FROM Album");

            ArrayList<Album> retour = new ArrayList<>();

            while (result.next()) {
                Album a = new Album(result.getObject(2).toString(),(int) result.getObject(3),result.getObject(4).toString(),result.getObject(5).toString());
                int id  = Integer.parseInt(result.getObject(1).toString());

                listeAlbums.put(id, a);
                retour.add(a);
            }

            result.close();
            sql.close();

            return retour;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retourne le Album qui possède l'id i en BDD
     * @param i id
     * @return Album
     **/
    public static Album getAlbumFromId(int i) {
        if(listeAlbums.isEmpty()) getAlbums();

        return listeAlbums.get(i);
    }

    /**
     * Retourne l'id du Album en fonction de son nom
     * @param a Album
     * @return id
     **/
    public static int getIdFromAlbum(Album a) {
        if(listeAlbums.isEmpty()) getAlbums();

        for (Entry<Integer, Album> unAlbum : listeAlbums.entrySet()) {
            if (unAlbum.getValue().equals(a)) {
                return unAlbum.getKey();
            }
        }
        return 0;
    }

    /**
     * Ajoute un album dans la BDD
     * @param a l'album à rajouter
     * @throws BDException erreur en BDD
     */
    public static void creerAlbum(Album a) throws BDException{
        if(listeAlbums.isEmpty()) getAlbums();
        if(!listeAlbums.containsValue(a)){
            String requete = "INSERT INTO Album (Nom,Nb_piste,Type,Couverture) VALUES (?,?,?,?);";
            PreparedStatement prepare;
            try {
                prepare = ConnexionBD.getInstance().prepareStatement(requete);

                prepare.setString(1, Utile.capitaliserTitre(a.getNom()));
                prepare.setInt(2, a.getNbrePistes());
                prepare.setString(3, a.getType());
                prepare.setString(4, a.getCouverture());
                prepare.executeUpdate();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            // MaJ à jour de la liste
            getAlbums();
        } else {
            throw new BDException(a + " existe déjà dans la base de données");
        }
    }

    /**
     * Affiche la liste d'Album.
     */
    public static void afficher(){
        String s = "";
        for(Entry<Integer,Album> entry : listeAlbums.entrySet()) {
            Integer key = entry.getKey();
            Album value = entry.getValue();

            s += key + " => " + value +"\n";
        }
        System.out.println(s);
    }

    /**
     * Vérifie si la TreeMap est vide.
     * @return boolean
     */
    public static boolean isEmpty(){
        return listeAlbums.isEmpty();
    }
}