package modele;

import controlleur.musique.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;

/**
 * Classe GenreModele
 * Elle permet de faire la liaison entre la table Genre en BDD et l'objet Genre
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class GenreModele {
    private static TreeMap<Integer, Genre> listeGenre = new TreeMap<>();


    /**
     * Retourne tout les genres présents en BDD
     * @return ArrayList
     **/
    public static ArrayList<Genre> getGenres(){
        Statement sql;
        try {
            sql = ConnexionBD.getInstance().createStatement();

            ResultSet result = sql.executeQuery("SELECT * FROM Genre");

            ArrayList<Genre> retour = new ArrayList<>();

            while (result.next()) {
                Genre g = new Genre(result.getObject(2).toString());
                int id  = Integer.parseInt(result.getObject(1).toString());

                listeGenre.put(id, g);
                retour.add(g);
            }

            result.close();
            sql.close();

            return retour;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retourne le Genre qui possède l'id i en BDD
     * @param i id
     * @return Genre
     **/
    public static Genre getGenreFromId(int i) {
        if(listeGenre.isEmpty()) getGenres();

        return listeGenre.get(i);
    }

    /**
     * Retourne l'id du genre en fonction de son nom
     * @param g Genre
     * @return id
     **/
    public static int getIdFromGenre(Genre g) {
        if(listeGenre.isEmpty()) getGenres();

        for (Entry<Integer, Genre> unGenre : listeGenre.entrySet()) {
            if (unGenre.getValue().equals(g)) {
                return unGenre.getKey();
            }
        }
        return -1;
    }

    /**
     * Vérifie si la TreeMap est vide.
     * @return boolean
     */
    public static boolean isEmpty(){
        return listeGenre.isEmpty();
    }
}