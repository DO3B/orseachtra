package modele;

/**
 * Classe BDException
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class BDException extends Exception {
    /**
     * Constructeur de BDException
     * @param message message à afficher
     */
    public BDException(String message){
        super(message);
    }
}
