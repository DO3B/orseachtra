package modele;

import controlleur.Utile;
import controlleur.musique.Musique;

import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.util.*;
import java.util.Map.Entry;

/**
 * Classe MusiqueModele
 * Elle permet de faire la liaison entre la table Musique en BDD et l'objet Musique
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class MusiqueModele {
    private static TreeMap<Integer, Musique> listeMusiques = new TreeMap<>();


    /**
     * Retourne tout les Musiques présents en BDD
     * @return ArrayList
     **/
    public static ArrayList<Musique> getMusiques(){
        listeMusiques = new TreeMap<>();
        Date date = null;
        SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
        Statement sql;
        try {
            sql = ConnexionBD.getInstance().createStatement();
            String requete = "SELECT ID, ID_Genre, Nom, ID_Artiste, ID_Album, Année, Durée, Note, Lien " +
                    "FROM Musique;";
            ResultSet result = sql.executeQuery(requete);

            ArrayList<Musique> retour = new ArrayList<>();

            if(GenreModele.isEmpty())GenreModele.getGenres();
            if(ArtisteModele.isEmpty())ArtisteModele.getArtistes();
            if(AlbumModele.isEmpty())AlbumModele.getAlbums();

            while (result.next()) {
                try {
                    date = formatAnnee.parse(result.getObject(6).toString());
                }catch (ParseException e){
                    e.printStackTrace();
                }
                Musique m = new Musique( GenreModele.getGenreFromId(Integer.parseInt(result.getObject(2).toString())),
                        result.getObject(3).toString(),
                        ArtisteModele.getArtisteFromId(Integer.parseInt(result.getObject(4).toString())),
                        AlbumModele.getAlbumFromId(Integer.parseInt(result.getObject(5).toString())),
                        date,
                        result.getObject(7).toString().charAt(0),
                        Integer.parseInt(result.getObject(8).toString()),
                        result.getObject(9).toString()
                        );
                int id  = Integer.parseInt(result.getObject(1).toString());

                listeMusiques.put(id, m);
                retour.add(m);
            }

            result.close();
            sql.close();

            return retour;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retourne tout les Musiques présents en BDD selon les parametres
     * @param mu musique de référence
     * @return ArrayList Musique
     **/
    public static ArrayList<Musique> getMusiquesRessemblantA(Musique mu){
        double pourcentageRessemblanceMin = 0.6;
        Date date = null;
        SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
        Statement sql;
        try {
            if(GenreModele.isEmpty())GenreModele.getGenres();

            sql = ConnexionBD.getInstance().createStatement();
            String requete = "SELECT ID_Genre, M.Nom, ID_Artiste, ID_Album, Année, Durée, Note, Lien " +
                    "FROM Musique M INNER JOIN Album D ON ID_Album = D.ID INNER JOIN Artiste A ON ID_Artiste = A.ID ";
            String param = "Where ID_Genre = '" + GenreModele.getIdFromGenre(mu.getGenre()) + "'";

            if(mu.getNom() != null) param += " AND ( M.Nom LIKE '%" + mu.getNom() +"%' OR JARO_WINKLER_SIMILARITY(M.Nom,'" + mu.getNom() +"') > " + pourcentageRessemblanceMin + ")";
            if(mu.getArtiste() != null) param += " AND ( A.Nom LIKE '%" + mu.getArtiste().getNom() +"%' OR JARO_WINKLER_SIMILARITY(A.Nom,'" + mu.getArtiste().getNom() +"') > " + pourcentageRessemblanceMin + ")";
            if(mu.getAlbum() != null) param += " AND ( D.Nom LIKE '%" + mu.getAlbum().getNom() +"%' OR JARO_WINKLER_SIMILARITY(D.Nom,'" + mu.getAlbum().getNom() +"') > " + pourcentageRessemblanceMin + ")";
            if(mu.getAnnee() != null){
                LocalDate localDate = mu.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                int annee = localDate.getYear();
                param += " AND Année BETWEEN "+ annee +"-2 AND "+ annee +"+2" ;
            }
            //if(mu.getDuree() != 'v') param += " AND Durée = '" + mu.getDuree() + "'";
            if(mu.getNote() != -1) param += " AND Note = " + mu.getNote();

            requete = requete + param + " ORDER BY M.Nom;";

            System.out.println(requete);

            ResultSet result = sql.executeQuery(requete);

            ArrayList<Musique> retour = new ArrayList<>();

            while (result.next()) {
                try {
                    date = formatAnnee.parse(result.getObject(5).toString());
                }catch (ParseException e){
                    e.printStackTrace();
                }
                Musique m = new Musique( GenreModele.getGenreFromId(Integer.parseInt(result.getObject(1).toString())),
                        result.getObject(2).toString(),
                        ArtisteModele.getArtisteFromId(Integer.parseInt(result.getObject(3).toString())),
                        AlbumModele.getAlbumFromId(Integer.parseInt(result.getObject(4).toString())),
                        date,
                        result.getObject(6).toString().charAt(0),
                        Integer.parseInt(result.getObject(7).toString()),
                        result.getObject(8).toString()
                );

                retour.add(m);
            }

            result.close();
            sql.close();

            if( retour.size() < 5 ) {
                retour = getMusiquesRessemblantA(new Musique(mu.getGenre()));
            }

            return retour;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retourne le Musique qui possède l'id i en BDD
     * @param i id
     * @return Musique
     **/
    public static Musique getMusiqueFromId(int i) {
        if(listeMusiques.isEmpty()) getMusiques();

        return listeMusiques.get(i);
    }

    /**
     * Retourne l'id du Musique en fonction de son nom
     * @param m Musique
     * @return id
     **/
    public static int getIdFromMusique(Musique m) {
        if(listeMusiques.isEmpty()) getMusiques();

        for (Entry<Integer, Musique> uneMusique : listeMusiques.entrySet()) {
            if (uneMusique.getValue().equals(m)) {
                return uneMusique.getKey();
            }
        }
        return 0;
    }

    /**
     * Ajoute la musique m dans la base de données.
     * @param m Musique
     * @throws BDException erreur en BDD
     */
    public static void creerMusique(Musique m) throws BDException{
        if(listeMusiques.isEmpty()) getMusiques();
        if(!listeMusiques.containsValue(m)){
            if(GenreModele.isEmpty())GenreModele.getGenres();
            if(ArtisteModele.isEmpty())ArtisteModele.getArtistes();
            if(AlbumModele.isEmpty())AlbumModele.getAlbums();

            LocalDate localDate = m.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int annee = localDate.getYear();

            String requete = "INSERT INTO Musique (Nom, Année, Durée, Note, Lien, ID_Artiste, ID_Genre, ID_Album) VALUES (?,?,?,?,?,?,?,?);";
            PreparedStatement prepare;
            System.out.println(AlbumModele.getIdFromAlbum(m.getAlbum()));
            try {
                prepare = ConnexionBD.getInstance().prepareStatement(requete);

                prepare.setString(1, Utile.capitaliserTitre(m.getNom()));
                prepare.setInt(2, annee);
                prepare.setString(3, m.getDuree()+"");
                prepare.setInt(4, m.getNote());
                prepare.setString(5, m.getLien());
                prepare.setInt(6, ArtisteModele.getIdFromArtiste(m.getArtiste()));
                prepare.setInt(7, GenreModele.getIdFromGenre(m.getGenre()));
                prepare.setInt(8, AlbumModele.getIdFromAlbum(m.getAlbum()));

                prepare.executeUpdate();
            }
            catch (SQLException e) {
                throw new BDException(e.getMessage());
            }
            getMusiques();
        } else {
            throw new BDException(m + " existe déjà dans la base de données.");
        }

    }

    /**
     * Remplace mOld par mNew dans la base de données.
     * @param mOld musique à remplacer
     * @param mNew nouvelle musique
     * @throws BDException erreur en BDD
     */
    public static void modifierMusique(Musique mOld, Musique mNew) throws BDException{
        if (listeMusiques.isEmpty()) getMusiques();

        if (GenreModele.isEmpty()) GenreModele.getGenres();
        if (ArtisteModele.isEmpty()) ArtisteModele.getArtistes();
        if (AlbumModele.isEmpty()) AlbumModele.getAlbums();

        if (!listeMusiques.containsValue(mNew)) {
            LocalDate localDate = mNew.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int annee = localDate.getYear();

            String requete = "UPDATE Musique SET Nom = ?, Année = ?, Durée = ?, Note = ?, Lien = ?, ID_Artiste = ?, ID_Genre = ?, ID_Album = ? Where ID = ?;";
            PreparedStatement prepare;
            try {
                prepare = ConnexionBD.getInstance().prepareStatement(requete);

                prepare.setString(1, Utile.capitaliserTitre(mNew.getNom()));
                prepare.setInt(2, annee);
                prepare.setString(3, mNew.getDuree() + "");
                prepare.setInt(4, mNew.getNote());
                prepare.setString(5, mNew.getLien());
                prepare.setInt(6, ArtisteModele.getIdFromArtiste(mNew.getArtiste()));
                prepare.setInt(7, GenreModele.getIdFromGenre(mNew.getGenre()));
                prepare.setInt(8, AlbumModele.getIdFromAlbum(mNew.getAlbum()));
                prepare.setInt(9, getIdFromMusique(mOld));

                prepare.executeUpdate();
            } catch (SQLException e) {
                throw new BDException(e.getMessage());
            }
            getMusiques();
        } else {
            throw new BDException(mNew + " existe déjà dans la base de données.");
        }
    }


    /**
     * Supprime la musique de la base de données.
     * @param m musique
     */
    public static void supprimerMusique(Musique m){
        if(listeMusiques.isEmpty()) getMusiques();

        String requete = "DELETE FROM Musique Where ID = ?;";
        PreparedStatement prepare;

        try {
            prepare = ConnexionBD.getInstance().prepareStatement(requete);

            prepare.setInt(1, getIdFromMusique(m));

            prepare.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        getMusiques();
    }
}