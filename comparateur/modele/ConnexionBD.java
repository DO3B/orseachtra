package modele;

import java.sql.*;

/**
 * Singleton pour la connexion à la base de données, fournit un accès unique.
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public final class ConnexionBD{
    private static final String user = "mayol_java";
    private static final String password = "mdpsupersecret";
    private static final String url = "jdbc:mariadb://mysql-mayol.alwaysdata.net:3306/mayol_orsearchtra";

    private static volatile Connection connexionBD = null;

    /**
     * Retourne l'instance unique de connexionBD
     * @return connexionBD
     */
    public static Connection getInstance(){
        if(connexionBD == null){
            new ConnexionBD();
        }
        return connexionBD;
    }

    /**
     * Constructeur se connectant à la BDD
     */
    private ConnexionBD(){
        try{
            connexionBD = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}