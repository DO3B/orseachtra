package modele;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Classe AdministrationBD
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class AdministrationBD {

    /**
     * Vérifie si le mdp Admin rentré est correct.
     * @param mdp de l'administrateur
     * @return true si le mdp entré est correct
     */
    public static boolean isMDPCorrect(String mdp){
        try{
            Statement sql;
            sql = ConnexionBD.getInstance().createStatement();
            ResultSet result = sql.executeQuery("SELECT * FROM Admin Where password = PASSWORD('"+ mdp + "');");
            return result.next();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}
