package controlleur;

import vue.FenetrePrincipale;

/**
 * Class Main
 *
 *  @author Loïc MAYOL - Marius RIDEL
 *  @version 0.1
*/
public class Main{

    /**
     * Fonction main
     * @param args arguments pour le main
     */
    public static void main(String[] args) {
        FenetrePrincipale fa = new FenetrePrincipale();
    }
}

