package controlleur;

import controlleur.musique.Musique;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;
import org.apache.commons.text.similarity.JaroWinklerDistance;

/**
 * Classe Comparateur
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Comparateur implements Comparator<Musique>{
    protected Musique ref;

    /**
     * Constructeur de Comparateur
     * @param m musique de référence du comparateur
     */
    public Comparateur(Musique m){
        ref = m;
    }

    /**
     * Compare la musique passée en paramètre à la musique de référence.
     * @param musiqueBDD musique à comparer
     * @return int score de comparaison
     */
    public int comparerMusique(Musique musiqueBDD){
        JaroWinklerDistance jwDistance = new JaroWinklerDistance();


        int resultat = 0;

        if(ref.getDuree() != 'v' && musiqueBDD.getDuree() == ref.getDuree()){
            resultat += 20;
        }

        if(ref.getNom() != null){
            resultat += 80 * jwDistance.apply(musiqueBDD.getNom(), ref.getNom());
        }

        if(ref.getArtiste() != null ){
            resultat += 50 * jwDistance.apply(musiqueBDD.getArtiste().getNom(), ref.getArtiste().getNom());
        }

        if(ref.getAlbum() != null){
            resultat += 40 * jwDistance.apply(musiqueBDD.getAlbum().getNom(), ref.getAlbum().getNom());
        }

        if(ref.getAnnee() != null){
            LocalDate localDate = musiqueBDD.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int anneeBDD = localDate.getYear();

            localDate = ref.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int anneeRef = localDate.getYear();

            resultat += 10 -(4 * Math.abs(anneeBDD - anneeRef) );

        }

        if(ref.getNote() != -1  && musiqueBDD.getNote() == ref.getNote()){
            resultat += 20;
        }

        resultat += musiqueBDD.getNote();

        return resultat;
    }

    @Override
    public int compare(Musique m1, Musique m2){
        return this.comparerMusique(m2) - this.comparerMusique(m1);
    }
}