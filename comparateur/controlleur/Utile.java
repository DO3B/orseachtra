package controlleur;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Classe Utile
 * Elle contient quelques utilitaires
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Utile {

    /**
     * Capitalise la chaîne de caractère.
     * @param str chaîne à capitaliser
     * @return la chaîne de caractère capitalisée
     */
    public static String capitaliserTitre(String str) {
        str = str.toLowerCase();
        StringBuffer s = new StringBuffer();
        char ch = ' ';
        for (int i = 0; i < str.length(); i++) {
            if (ch == ' ' && str.charAt(i) != ' ')
                s.append(Character.toUpperCase(str.charAt(i)));
            else
                s.append(str.charAt(i));
            ch = str.charAt(i);
        }
        str = s.toString();
        return str.trim();
    }

    /**
     * Permet d'ouvrir le lien sur le navigateur internet par défault de l'utilisateur.
     * @param lien lien à ouvrir
     */
    public static void ouvrirLien(String lien){
        try {
            Desktop.getDesktop().browse(new URL(lien).toURI());
        } catch (IOException | URISyntaxException e1) {
            JOptionPane.showMessageDialog(new JFrame(),
                    "Le lien est actuellement indisponible.",
                    "Lien indisponible",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Convertit le caractère c en une chaîne de caractères correspondantes.
     * @param c caractère
     * @return String
     */
    public static String convertirDuree(char c){
        String duree;
        switch (c){
            case 'c':
                duree = "Courte";
                break;
            case 'm':
                duree = "Moyenne";
                break;
            case 'l':
                duree = "Longue";
                break;
            default:
                duree = "Non précisée";
                break;
        }

        return duree;
    }

    /**
     * Enlève ceretains caractères spéciaux et capitalise une chaîne
     * @param s chaîne à formatter
     * @return chaîne formattée
     */
    public static String formatter(String s){
        s = s.replace("\'", "");
        s = s.replace("%", "");
        s = s.replace("\"", "");
        s = s.replace(";", "");
        s = capitaliserTitre(s);
        return s;
    }
}
