package controlleur;

import modele.AdministrationBD;

/**
 * Classe Admin
 *
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Admin {
    private String mdp;

    /**
     * Constructeur d'Admin
     * @param mdp mot de passe de l'admin
     */
    public Admin(String mdp){
        this.mdp = mdp;
    }

    /**
     * Vérifie si le mot de passe est correct.
     * @return boolean
     */
    public boolean isMDPCorrect(){
        return AdministrationBD.isMDPCorrect(this.mdp);
    }
}
