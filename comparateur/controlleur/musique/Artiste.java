package controlleur.musique;

import java.util.ArrayList;

import controlleur.Utile;
import modele.ArtisteModele;
import modele.BDException;
import vue.FenetrePopup;

/**
 * Classe Artiste
 * 
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Artiste {
    private static ArrayList<Artiste> listeArtiste = new ArrayList<>();
    private String nom;

    /**
     * Constructeur de base d'Artiste
     * @param n nom de l'artiste
     */
    public Artiste(String n){
        this.nom = n;
    }

    /**
     * Constructeur par copie d'Artiste
     * @param a artiste
     */
    public Artiste(Artiste a){
        this.nom = a.nom;
    }

    /**
     * @return le nom
     */
    public String getNom(){
        return nom;
    }

    /**
     * Set le nom
     * @param nom nom de l'artiste
     */
    public void setNom(String nom){
        this.nom = nom;
    }

    @Override
    public String toString(){
        return nom;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Artiste){
            Artiste a = (Artiste) o;
            return Utile.capitaliserTitre(this.nom).equals( Utile.capitaliserTitre( a.getNom() ) );
        } else {
            return  false;
        }
    }

    /**
     * Remplie listeArtiste si elle est vide et la retourne.
     * @return ArrayList listeArtiste, liste des Genres
     */
    public static ArrayList<Artiste> getAllArtistes(){
        if(listeArtiste.isEmpty()){
            majajourArtiste();
        }
        return  listeArtiste;
    }

    /**
     * Met à jour la listeArtiste.
     */
    public static void majajourArtiste(){
        listeArtiste = ArtisteModele.getArtistes();
    }

    /**
     * Ajoute l'objet dans la base de données.
     */
    public void ajouterALaBDD(){
        try {
            ArtisteModele.creerArtiste(this);
            new FenetrePopup(this + " a bien été ajouté à la base de données", false);
        } catch (BDException e){
            new FenetrePopup(e.getMessage(), true);
        }
    }

}