package controlleur.musique;

import java.util.ArrayList;

import modele.GenreModele;


/**
 * Classe Genre
 * 
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Genre {
    private static ArrayList<Genre> listeGenre = new ArrayList<>();
    private String nom;

    /**
     * Constructeur de Genre
     * @param n nom du genre
     */
    public Genre(String n){
        this.nom = n;
    }

    /**
     * Constructeur par copie de Genre
     * @param g genre
     */
    public Genre(Genre g){
        this.nom = g.nom;
    }

    /**
     * @return le nom
     */
    public String getNom(){
        return nom;
    }

    @Override
    public String toString(){
        return nom;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Genre){
            Genre g = (Genre) o;
            return this.nom.equals(g.getNom());
        } else {
            return  false;
        }
    }

    /**
     * Remplie listeGenre si elle est vide et la retourne.
     * @return ArrayList listeGenre, liste des Genres
     */
    public static ArrayList<Genre> getAllGenres(){
        if(listeGenre.isEmpty()){
            majajourGenre();
        }
        return  listeGenre;
    }

    /**
     * Met à jour la listeGenre.
     */
    public static void majajourGenre(){
        listeGenre = GenreModele.getGenres();
    }
}