package controlleur.musique;

import controlleur.Utile;
import modele.AlbumModele;
import modele.BDException;
import vue.FenetrePopup;

import java.util.ArrayList;

/**
 * Classe Album
 * 
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Album {
    private static ArrayList<Album> listeAlbum  = new ArrayList<>();

    private String nom;
    private int nbrePistes;
    private String type;
    private String couverture;

    /**
     * Constructeur de base d'Album
     * @param n nom de l'album
     * @param np le nombre de pistes musicales
     * @param t le type d'album ("Ep","Single", ...)
     * @param u lien vers la pochette de l'album
     */
    public Album(String n, int np, String t, String u){
        this.nom = n;
        this.nbrePistes = np;
        this.type = t;
        this.couverture = u;
    }

    /**
     * Constructeur d'Album
     * @param n nom de l'album
     */
    public Album(String n){
        this.nom = n;
        this.nbrePistes = -1;
        this.type = null;
        this.couverture = null;
    }

    /**
     * Constructeur par copie d'Album
     * @param a album
     */
    public Album(Album a){
        this.nom = a.nom;
        this.nbrePistes = a.nbrePistes;
        this.type = a.type;
        this.couverture = a.couverture;
    }

    /**
     * @return le nom
     */
    public String getNom(){
        return nom;
    }

    /**
     * @return le nombre de morceaux
     */
    public int getNbrePistes() {
        return nbrePistes;
    }

    /**
     * @return le type
     */
    public String getType() {
        return type;
    }

    /**
     * Set le nom
     * @param nom nom de l'album
     */
    public void setNom(String nom){
        this.nom = nom;
    }

    /**
     * @return la couverture
     */
    public String getCouverture() {
        return couverture;
    }

    @Override
    public String toString(){
        return nom;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Album){
            Album a = (Album) o;
            return (
                    Utile.capitaliserTitre(this.nom).equals(Utile.capitaliserTitre(a.getNom()))
                    && this.type.equals(a.getType())
                    && this.nbrePistes == a.getNbrePistes()
                    );
        } else {
            return  false;
        }
    }

    /**
     * Remplie listeAlbum si elle est vide et la retourne.
     * @return ArrayList listeAlbum, liste des Genres
     */
    public static ArrayList<Album> getAllAlbums(){
        if(listeAlbum.isEmpty()){
            majajourAlbum();
        }
        return  listeAlbum;
    }

    /**
     * Met à jour la listeGenre.
     */
    public static void majajourAlbum(){
        listeAlbum = AlbumModele.getAlbums();
    }

    /**
     * Ajoute l'objet dans la base de données
     */
    public void ajouterALaBDD(){
        try{
            AlbumModele.creerAlbum(this);
            new FenetrePopup(this + " a bien été ajouté dans la base de données", false);
        } catch (BDException e){
            new FenetrePopup(e.getMessage(), true);
        }
    }
}