package controlleur.musique;

import controlleur.Utile;
import modele.BDException;
import modele.MusiqueModele;
import vue.FenetrePopup;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
/**
 * Classe Musique
 * La comparaison s'effectue sur ce type d'objet.
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Musique {
    private String nom;
    private Artiste artiste;
    private Album album;
    private Genre genre;
    private Date annee;
    private char duree;
    private int note;
    private String lien;

    /**
     * Constructeur de Musique
     * @param g genre de la musique
     */
    public  Musique(Genre g){
        this.nom = null;
        this.artiste = null;
        this.album = null;
        this.genre = g;
        this.annee = null;
        this.duree = 'v';
        this.note = -1;
        this.lien = null;
    }

    /**
     * Constructeur de base de Musique
     * @param g genre de la musique
     * @param n nom de la musique
     * @param ar artiste de la musique
     * @param al album de la musique
     * @param an année de la musique
     * @param dur durée de la musique
     * @param not note de la musique
     */
    public  Musique(Genre g, String n, Artiste ar, Album al, Date an, char dur, int not){
        this.nom = n;
        this.artiste = ar;
        this.album = al;
        this.genre = g;
        this.annee = an;
        this.duree = dur;
        this.note = not;
        this.lien = null;
    }

    /**
     * Constructeur complet de Musique
     * @param g genre de la musique
     * @param n nom de la musique
     * @param ar artiste de la musique
     * @param al album de la musique
     * @param an année de la musique
     * @param dur durée de la musique
     * @param not note de la musique
     * @param link lien vers la musique
     */
    public  Musique(Genre g, String n, Artiste ar, Album al, Date an, char dur, int not, String link){
        this.nom = n;
        this.artiste = ar;
        this.album = al;
        this.genre = g;
        this.annee = an;
        this.duree = dur;
        this.note = not;
        this.lien = link;
    }

    /**
     * Constructeur par copie de Musique
     * @param m musique à copier
     */
    public Musique(Musique m){
        this.nom = m.nom;
        this.artiste = new Artiste(m.artiste);
        this.album = new Album(m.album);
        this.genre = new Genre(m.genre);
        this.annee = m.annee;
        this.duree = m.duree;
        this.note = m.note;
        this.lien = m.lien;
    }

    /**
     * @return le nom
     */
    public String getNom(){
        return nom;
    }

    /**
     * @return l'album
     */
    public Album getAlbum() {
        return album;
    }

    /**
     * @return l'artiste
     */
    public Artiste getArtiste() {
        return artiste;
    }
    /**
     * @return le genre
     */
    public Genre getGenre() {
        return genre;
    }

    /**
     * @return l'année
     */
    public Date getAnnee() {
        return annee;
    }

    /**
     * @return la durée
     */
    public char getDuree() {
        return duree;
    }

    /**
     * @return le lien
     */
    public String getLien() {
        return lien;
    }

    /**
     * @return la note
     */
    public int getNote() {
        return note;
    }

    /**
     * Set le nom
     * @param nom titre de la musique
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Set l'artiste
     * @param artiste artiste réalisant la musique
     */
    public void setArtiste(Artiste artiste) {
        this.artiste = artiste;
    }

    /**
     * Set l'album
     * @param album album contenant la musique
     */
    public void setAlbum(Album album) {
        this.album = album;
    }

    /**
     * Set le genre
     * @param genre genre de la musique
     */
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * Set l'année
     * @param annee année de publication
     */
    public void setAnnee(Date annee) {
        this.annee = annee;
    }

    /**
     * Set la durée
     * @param duree durée de la musique
     */
    public void setDuree(char duree) {
        this.duree = duree;
    }

    /**
     * Set la note
     * @param note note de la musique
     */
    public void setNote(int note) {
        this.note = note;
    }

    /**
     * Set le lien
     * @param lien lien versle site d'écoute
     */
    public void setLien(String lien) {
        this.lien = lien;
    }

    @Override
    public String toString(){
        return nom + " - " + artiste.getNom();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Musique){
            Musique m = (Musique) o;

            LocalDate localDate = m.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int anneeM = localDate.getYear();
            localDate = this.getAnnee().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int anneeT = localDate.getYear();

            return(Utile.capitaliserTitre(this.nom).equals(Utile.capitaliserTitre(m.getNom()))
             && this.artiste.equals(m.getArtiste())
             && this.album.equals(m.getAlbum())
             && this.genre.equals(m.getGenre())
             && anneeT == anneeM
             && this.duree == m.getDuree()
             && this.note == m.getNote()
            );

        } else {
            return  false;
        }
    }

    /**
     * Ajoute l'objet dans la base de données.
     */
    public void ajouterALaBDD(){
        for(Album a : Album.getAllAlbums()){
            if(a.getNom().equals(this.album.getNom())){
                this.album = a;
            }
        }

        try {
            MusiqueModele.creerMusique(this);
            new FenetrePopup(this + " a bien été ajoutée dans la base de données", false);
        } catch (BDException e) {
            new FenetrePopup(e.getMessage(), true);
        }


    }

    /**
     * Modifie en base de données l'objet par celui en paramètre.
     * @param musiqueMAJ nouvelle version de la musique
     */
    public void modifierDansLaBDD(Musique musiqueMAJ){
        for(Album a : Album.getAllAlbums()){
            if(a.getNom().equals(musiqueMAJ.album.getNom())){
                musiqueMAJ.album = a;
            }
        }
        try {
            MusiqueModele.modifierMusique(this, musiqueMAJ);
            new FenetrePopup("Modification de la base de données effectuée !", false);
        } catch (BDException e){

            new FenetrePopup(e.getMessage(), true);
        }


    }

    /**
     * Supprime l'objet de la base de données.
     */
    public void supprimerDansLaBDD(){
        MusiqueModele.supprimerMusique(this);
    }
}