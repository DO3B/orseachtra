package controlleur;

import controlleur.musique.Musique;
import modele.MusiqueModele;
import vue.FenetreResultats;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Classe Recherche
 * Elle permet de lancer notre comparaison et de retourner un résultat
 * @author Loïc MAYOL - Marius RIDEL
 * @version 1.0
 */
public class Recherche {

    /**
     * Retourne les n meilleurs résultats triés.
     * @param musiqueR musique de référence
     * @param nbDeRetour nombre de résultat à retourner
     * @return ArrayList Musique résultats de la recherche
     */
    public static ArrayList<Musique> lancerRecherche(Musique musiqueR, int nbDeRetour){
        if(musiqueR.getNom() != null) musiqueR.setNom(Utile.formatter(musiqueR.getNom()));
        if(musiqueR.getArtiste() != null) musiqueR.getArtiste().setNom(Utile.formatter(musiqueR.getArtiste().getNom()));
        if(musiqueR.getAlbum() != null) musiqueR.getAlbum().setNom(Utile.formatter(musiqueR.getAlbum().getNom()));

        Comparateur c;

        c = new Comparateur(musiqueR);

        ArrayList<Musique> listeMusique = MusiqueModele.getMusiquesRessemblantA(musiqueR);
        Collections.sort(listeMusique,c);

        // ArrayList contenant uniquement les nbDeRetour meilleurs résultats
        if(nbDeRetour == -1) return listeMusique;

        ArrayList<Musique> resultatMusique = new ArrayList<>();
        int i = 0;
        while (i < nbDeRetour && i < listeMusique.size()){
            System.err.println(c.comparerMusique(listeMusique.get(i)) + " - " + listeMusique.get(i));
            resultatMusique.add(listeMusique.get(i));
            i++;
        }

        return resultatMusique;
        //new FenetreResultats(resultatMusique);
    }
}
